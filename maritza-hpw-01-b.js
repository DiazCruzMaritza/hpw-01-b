/********MARITZA DIAZ CRUZ***********/

/*********************GRUPOS*********************/

var grupo= function(_clave, _nombre, _docentes, _alumnos, _materias)
{
	return {
	"clave": _clave,
	"nombre": _nombre,
	"docentes": _docentes,
	"alumnos": _alumnos,
	"materias": _materias
	};
};


function asignarMateriasAlGrupo(g,m){
    if (!g.materias){
        g.materias = [];
}
if(!existeMateriasEnGrupo(g,m.clave)){
    g.materias.push(m);
    return true;
}
    return false;
}


function asignarAlumnosAlGrupo(g,a){
    if (!g.alumnos){
        g.alumnos = [];
}
if(!existeAlumnosEnGrupo(g,a.clave)){
    g.alumnos.push(a);
    return true;
}
    return false;
}

function asignarDocentesAlGrupo(g,d){
    if (!g.docentes){
        g.docentes = [];
}
if(!existeDocentesEnGrupo(g,d.clave)){
    g.docentes.push(d);
    return true;
}
    return false;
}

/***CRUD DE INSERTAR***/

function existeMateriasEnGrupo(g,cm){
  if (!g.materias || g.materias.length===0){
        return false;
}
    for (var i=0; i<g.materias.length; i++){
        if(g.materias[i].clave===cm){
            return true;
        }
    }
    return false;
}


function existeAlumnosEnGrupo(g,ca){
  if (!g.alumnos || g.alumnos.length===0){
        return false;
}
    for (var i=0; i<g.alumnos.length; i++){
        if(g.alumnos[i].clave===ca){
            return true;
        }
    }
    return false;
}

function existeDocentesEnGrupo(g,cd){
  if (!g.docentes|| g.docentes.length===0){
        return false;
}
    for (var i=0; i<g.docentes.length; i++){
        if(g.docentes[i].clave===cd){
            return true;
        }
    }
    return false;
}


var grupo1=grupo("1213", "ISA");


/************************DOCENTES**********************************/

var docentes= function(_clave, _nombre, _apellidos, _gradoacademico)
{
	return {
	"clave": _clave,
	"nombre": _nombre,
	"apellidos": _apellidos,
	"gradoacademico": _gradoacademico
	};
};


function asignarMateriasAlDocente(g,m){
    if (!g.docentes){
        g.docentes = [];
}
if(!existeMateriasEnAlumno(g,m.clave)){
    g.docentes.push(m);
    return true;
}
    return false;
}

var docente1=docentes("1432", "ANTONIO", "HERNANDEZ BLAS", "INGENIERO");
var docente2=docentes("6742", "CARLOS", "FLORES", "TECNICO");


/***CRUD DE INSERTAR***/

function existeMateriasEnDocente(g,cm){
  if (!g.docentes || g.docentes.length===0){
        return false;
}
    for (var i=0; i<g.docentes.length; i++){
        if(g.docentes[i].clave===cm){ 
            return true;
        }
    }
    return false;
}


/***********************ALUMNOS**********************************/

var alumnos= function(_clave, _nombre, _apellidos, _calificaciones)
{
	return {
	"clave": _clave,
	"nombre": _nombre,
	"apellidos": _apellidos,
	"califcaciones": _calificaciones
	};
};


function asignarMateriasAlAlumno(g,m){
    if (!g.alumnos){
        g.alumnos = [];
}
if(!existeMateriasEnAlumno(g,m.clave)){
    g.alumnos.push(m);
    return true;
}
    return false;
}


/***CRUD DE INSERTAR***/

function existeMateriasEnAlumno(g,cm){
  if (!g.alumnos || g.alumnos.length===0){
        return false;
}
    for (var i=0; i<g.alumnos.length; i++){
        if(g.alumnos[i].clave===cm){
            return true;
        }
    }
    return false;
}


var alumno1=alumnos("111160021", "MARITZA", "DIAZ CRUZ", "90");
var alumno2=alumnos("101345353", "JOSE", "GOPAR LUJAN", "80");
var alumno3=alumnos("098121232", "JUANA", "CRUZ VALENZUELA", "70");
var alumno4=alumnos("123422456", "NADIA", "LUJAN CORTES", "100");


/***************************MATERIAS**********************************/

var materias= function(_clave, _nombre)
{
	return {
	"clave": _clave,
	"nombre": _nombre
	};
};


var materia1=materias("HPW101", "HERRAMIENTAS DE PROGRAMACION WEB");
var materia2=materias("CYER09", "CONMUTACION Y ENRUTAMIENTO DE REDES");


/***********************************************************************/
/*GRUPO*/

asignarMateriasAlGrupo(grupo1,materia1);
asignarMateriasAlGrupo(grupo1,materia2);

asignarAlumnosAlGrupo(grupo1,alumno1);
asignarAlumnosAlGrupo(grupo1,alumno2);
asignarAlumnosAlGrupo(grupo1,alumno3);
asignarAlumnosAlGrupo(grupo1,alumno4);

asignarDocentesAlGrupo(grupo1,docente1);
asignarDocentesAlGrupo(grupo1,docente2);

existeMateriasEnGrupo(grupo1,'HPW101');
existeMateriasEnGrupo(grupo1,'M10');

existeAlumnosEnGrupo(grupo1,'111160021');
existeAlumnosEnGrupo(grupo1,'1010');

existeDocentesEnGrupo(grupo1,'1432');
existeDocentesEnGrupo(grupo1,'6767');

/*DOCENTE*/

asignarMateriasAlDocente(docente1,materia1);
asignarMateriasAlDocente(docente1,materia2);

existeMateriasEnDocente(docente1,'HPW101');
existeMateriasEnDocente(docente1,'M10');

/*ALUMNO*/

asignarMateriasAlAlumno(alumno1,materia1);
asignarMateriasAlAlumno(alumno1,materia2);

existeMateriasEnAlumno(alumno1,'HPW101');
existeMateriasEnAlumno(alumno1,'M10');


grupo1;
materia1;
alumno1;
docente1;


/************CRUD DE MODIFICAR****************/

/*Para docente*/

function BuscarDocente(cd){
   for(var i = 0; i<docentes.length; i++){
     if(docentes[i].clave==cd){
       return i;
     }
   }
   return -1;
 }
 
function ActualizarDocente(cd, clave, nombre, apellidos, gradoacademico){
   var actualizar = BuscarDocente(cd);
   if(actualizar!=-1){
     docentes[actualizar].clave = clave;
     docentes[actualizar].nombre = nombre;
     docentes[actualizar].apellidos = apellidos;
     docentes[actualizar].gradoacademico = gradoacademico;
   }
   else{
     console.log("NO SE ENCUENTRA DICHO DOCENTE");
   }
 }
 
var docente1=docentes("1111", "Mario", "Hernandez Lopez","ING");

docente1

/*Para alumno*/

function BuscarAlumno(ca){
   for(var i = 0; i<alumnos.length; i++){
     if(alumnos[i].clave==ca){
       return i;
     }
   }
   return -1;
 }
 
function ActualizarAlumno(ca, clave, nombre, apellidos, calificaciones){
   var actualizar = BuscarAlumno(cd);
   if(actualizar!=-1){
     alumnos[actualizar].clave = clave;
     alumnos[actualizar].nombre = nombre;
     alumnos[actualizar].apellidos = apellidos;
     alumnos[actualizar].calificaciones = calificaciones;
   }
   else{
     console.log("NO SE ENCUENTRA DICHO DOCENTE");
   }
 }
 
var alumno1=alumnos("10106001", "Omar", "Cruz Perez","100");

alumno1;

/*Para materia*/

function BuscarMateria(cm){
   for(var i = 0; i<materias.length; i++){
     if(materias[i].clave==cm){
       return i;
     }
   }
   return -1;
 }
 
function ActualizarMateria(cm, clave, nombre){
   var actualizar = BuscarMateria(cm);
   if(actualizar!=-1){
     materias[actualizar].clave = clave;
     materias[actualizar].nombre = nombre;
   }
   else{
     console.log("NO SE ENCUENTRA DICHA MATERIA");
   }
 }
 
var materia1=materias("PLF", "PROGRAMACION LOGICA Y FUNCIONAL");

materia1;

grupo1;

/************CRUD DE ELIMINAR****************/

function EliminarDocente (g,cd){
   for (var i = 0; i<g.docentes.length; i++) {
       if (g.docentes[i]['clave'] === clave){
           g.docentes.splice(i,1);
            return g.docentes
        }
    }
    return 0;
}


function EliminarAlumno(g,cd){
   for (var i = 0; i<g.docentes.length; i++) {
       if (g.docentes[i]['clave'] === clave){
           g.docentes.splice(i,1);
            return g.docentes
        }
    }
    return 0;
}


function EliminarAlumno (g,ca){
   for (var i = 0; i<g.alumnos.length; i++) {
       if (g.alumnos[i]['clave'] === clave){
           g.alumnos.splice(i,1);
            return g.alumnos
        }
    }
    return 0;
}


function EliminarMateria (g,cm){
   for (var i = 0; i<g.materias.length; i++) {
       if (g.materias[i]['clave'] === clave){
           g.materias.splice(i,1);
            return g.materias
        }
    }
    return 0;
}